import Flutter
import UIKit

public class SwiftFlutterAdmobMediationFanPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_admob_mediation_fan", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterAdmobMediationFanPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
