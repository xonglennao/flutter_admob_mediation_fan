
import 'dart:async';

import 'package:flutter/services.dart';

class FlutterAdmobMediationFan {
  static const MethodChannel _channel =
      const MethodChannel('flutter_admob_mediation_fan');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
